NIVA UC1b Biodiversity Indicator
---------------------------------
---------------------------------

Introduction
------------
This sub-project is part of the NIVA_ project that delivers a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009.

Please visit the NIVA_ website for further information. A complete list of the sub-projects made available under the NIVA project can be found in github_


NIVA UC1b Biodiversity Indicator
---------------------------------
A precise documentation for installation and usage of the tool can be found at the path ./doc/NIVA UC1b Biodiversity Tier I User guide.v0.1.2.docx*

|EU-PL 1.2 shield|

The source code relative to this work is licensed under the EUPL-1.2-or-later.

|CC BY-SA 4.0 shield|

The dataset relative to this work is licensed under a `Creative Commons Attribution 4.0
International License <http://creativecommons.org/licenses/by-sa/4.0/>`__.

|CC BY-SA 4.0|


.. _NIVA: https://www.niva4cap.eu/
.. _github: https://gitlab.com/nivaeu/
.. |EU-PL 1.2 shield| image:: https://img.shields.io/badge/license-EUPL-green.svg
   :target: https://opensource.org/licenses/EUPL-1.2
.. |CC BY-SA 4.0 shield| image:: https://img.shields.io/badge/License-CC%20BY--SA%204.0-green.svg
   :target: http://creativecommons.org/licenses/by-sa/4.0/
.. |CC BY-SA 4.0| image:: https://licensebuttons.net/l/by-sa/4.0/88x31.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
